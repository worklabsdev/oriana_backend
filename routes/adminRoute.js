var express = require("express");
var multer = require("multer");
var upload = multer({ dest: "uploads/" });

var router = express.Router();
var adminCtrl = require("../controllers/admin/adminCtrl");
var userCtrl = require("../controllers/admin/userCtrl");
var sellerCtrl = require("../controllers/admin/sellerCtrl");

router.post("/", function(req, res) {
  req.status(200).render("admin/dist/index");
}),
  router.post("/login", adminCtrl.login),
  router.post("/logout", adminCtrl.logout),
  router.post("/info", adminCtrl.info),
  router.post("/auth", adminCtrl.auth),
  router.post("/changepassword", adminCtrl.changePassword);
router.post("/forgetpassword", adminCtrl.forgetPassword);

router.post("/user/add", userCtrl.add);
router.post("/user/update", userCtrl.update);
router.post("/user/delete", userCtrl.delete);
router.post("/user/activate", userCtrl.activate);
router.post("/user/inactivate", userCtrl.inactivate);
router.post("/users", userCtrl.list);
router.post("/user", userCtrl.get);

router.post("/seller/add", sellerCtrl.add);
router.post("/seller/update", sellerCtrl.update);
router.post("/seller/delete", sellerCtrl.delete);
router.post("/seller/activate", sellerCtrl.activate);
router.post("/seller/inactivate", sellerCtrl.inactivate);
router.post("/sellers", sellerCtrl.list);
router.post("/seller", sellerCtrl.get);

router.get("/getCategory/:cat_id", adminCtrl.getCategory);
router.get("/getCategories", adminCtrl.getCategories);

router.post("/deleteCategory", adminCtrl.deleteCategory);
router.post("/addCategory", adminCtrl.addCategory);
router.put("/updateCategory", adminCtrl.updateCategory);
// router.post('/seller',adminCtrl.get)

module.exports = router;
