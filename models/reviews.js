var database = require("../config/database");
var Schema = database.mongoose.Schema;

var reviewSchema = new Schema(
  {
    product_id: { type: Schema.Types.ObjectId, required: true, trim: true },
    buyerId: {
      type: Schema.Types.ObjectId,
      required: true,
      trim: true,
      ref: "buyers"
    },
    title: { type: String, required: false, trim: true },
    message: { type: String, required: false, trim: true },
    star: { type: String, required: false, trim: true }
  },
  { collection: "reviews" }
);

module.exports = database.mongoose.model("reviews", reviewSchema);
