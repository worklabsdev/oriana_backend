var database = require("../config/database");
var Schema = database.mongoose.Schema;

var buyerSchema = new Schema(
  {
    // status: { type: String, required: false, trim: true },
    name: { type: String, required: true, trim: true },
    email: { type: String, required: true, trim: true },
    password: { type: String, required: true },
    phone: { type: String, required: false, trim: true },
    image: { type: String, required: false, trim: true },
    confirmCode: { type: String, required: false },
    recoverCode: { type: String, required: false },
    token: { type: Array, required: false, default: [] },
    address: { type: String, required: false, trim: true },
    city: { type: String, required: false, trim: true },
    state: { type: String, required: false, trim: true },
    country: { type: String, required: false, trim: true },
    pincode: { type: String, required: false, trim: true },
    isApproved: { type: Boolean, required: true, trim: true, default: false },
    isConfirmed: { type: Boolean, required: true, trim: true, default: false },
    isBlocked: { type: Boolean, default: false, trim: true },
    isDeleted: { type: Boolean, required: true, trim: true, default: false },
    cart: [
      {
        product_id: { type: Schema.Types.ObjectId, ref: "products" },
        quantity: { type: Number },
        size: { type: String },
        color: { type: String }
      }
    ],
    wishlist: [
      {
        product_id: { type: Schema.Types.ObjectId, ref: "products" },
        quantity: { type: Number },
        size: { type: String },
        color: { type: String }
      }
    ]
  },
  { collection: "buyers" }
);

buyerSchema.statics.findByToken = function(token) {
  return this.findOne({ token: { $in: [token] }, isDeleted: false }).lean();
};

buyerSchema.statics.findByMail = function(email, phone) {
  return this.findOne({
    email: new RegExp(email, "i"),
    isDeleted: false
  }).lean();
};

buyerSchema.statics.findExistence = function(ep, password) {
  return this.findOne({
    $or: [
      { email: new RegExp(ep, "i"), password: password },
      { phone: new RegExp(ep, "i"), password: password }
    ],
    isDeleted: false
  }).lean();
};

buyerSchema.statics.removeimage = function(id, url) {
  this.update({ _id: id }, { $set: { image: url } }).exec(function(err, data) {
    if (err) throw err;
  });
};

module.exports = database.mongoose.model("buyers", buyerSchema);
