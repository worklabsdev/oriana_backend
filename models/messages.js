var database = require("../config/database");
var Schema = database.mongoose.Schema;

var msgSchema = new Schema(
  {
    buyerId: { type: String, required: true, trim: true },
    message: { type: String, required: false, trim: true },
    sender: { type: String, required: true, default: "buyer" },
    create_time: { type: Date, required: true, default: Date.now }
  },
  { collection: "messages" }
);

module.exports = database.mongoose.model("messages", msgSchema);
