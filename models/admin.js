var database = require("../config/database");
var Schema = database.mongoose.Schema;

var adminSchema = new Schema(
  {
    email: { type: String, required: true, trim: true },
    firstname: { type: String },
    lastname: { type: String, trim: true },
    password: { type: String, required: true },
    is_deleted: { type: Boolean, required: false, default: false },
    token: { type: String, required: false, default: null },
    create_time: { type: Date, default: Date.now },
    category: { type: String, required: true, ref: "categories" },
    subcategory: { type: String, required: true, ref: "subcategories" }
  },
  { collection: "admin" }
);
module.exports = database.mongoose.model("admin", adminSchema);
