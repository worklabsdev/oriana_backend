var adminMdl = require("../../models/admin.js");
var jsend = require("../../plugins/jsend.js");
var nodemailer = require("nodemailer");
var categoryMdl = require("../../models/categories");
var subcategoryMdl = require("../../models/subcategories");
//var emailtransport = require('../../config/email.js');

var loginAction = function(req, res) {
  if (req.body.email && req.body.email != "") {
    var email = req.body.email.toString().trim();
  } else {
    return res.status(400).failure("please Enter email");
  }

  if (req.body.password && req.body.password != "") {
    var password = req.body.password.toString();
  } else {
    return res.status(400).failure("please Enter password");
  }
  adminMdl
    .findOne({ email: new RegExp(email, "i"), password: password })
    .exec(function(err, data) {
      if (err) throw err;

      if (!data) {
        return res.status(400).failure("wrong username or password");
      } else {
        delete data.token;
        var token = "";
        var possible =
          "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 14; i++) {
          token += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        adminMdl
          .update({ _id: data._id }, { $set: { token: token } })
          .exec((err, adata) => {
            if (err) throw err;
            return res.success({ token: token }, "Logged in successfully");
          });
      }
    });
};

var logoutAction = function(req, res) {
  if (req.body.token || req.body.token != "") {
    var token = req.body.token;
    adminMdl.find({ token: token }).exec(function(err, data) {
      if (err) throw err;
      // console.log("data", data)
      if (data.length == 0) {
        return res.status(400).failure("Invalid Token");
      }
      adminMdl
        .update({ token: token }, { $unset: { token: null } })
        .exec((err, data) => {
          if (err) {
            throw err;
          } else {
            return res.success([], "successfully logged out");
          }
        });
    });
  } else {
    return res.status(400).failure("No Token Found");
  }
};

var infoAction = function(req, res) {
  if (req.body.token && req.body.token != "") {
    var token = req.body.token;
    adminMdl.find({ token: token }).exec(function(err, data) {
      if (err) throw err;
      if (data.length == 0) {
        return res.status(400).failure("Invalid Token");
      }
      return res.success(data, "Information");
    });
  } else {
    return res.status(400).failure("Invalid Token");
  }
};

function authAction(req, res) {
  if (req.body.token && req.body.token != "") {
    var token = req.body.token;
    adminMdl.find({ token: token }).exec(function(err, data) {
      if (err) throw err;
      if (data.length == 0) {
        return res.status(400).failure("Invalid Token");
      }
      return res.success([], "Valid Token");
    });
  } else {
    return res.status(400).failure("Token Not Found");
  }
}
var changePasswordAction = function(req, res) {
  if (req.body.oldpassword && req.body.oldpassword != "") {
    var oldpassword = req.body.oldpassword.toString();
  } else {
    return res.status(400).failure("please Enter old password");
  }

  if (req.body.newpassword && req.body.newpassword != "") {
    var newpassword = req.body.newpassword.toString();
  } else {
    return res.status(400).failure("please Enter New password");
  }

  if (req.body.cpassword && req.body.cpassword != "") {
    var cpassword = req.body.cpassword.toString();
  } else {
    return res.status(400).failure("please Enter Confirm password");
  }

  if (req.body.token) {
    adminMdl.find({ token: req.body.token }).exec(function(err, admindetail) {
      if (err) throw err;

      if (admindetail.length > 0) {
        if (admindetail[0].password != oldpassword) {
          return res.status(400).failure("Invalid old password");
        }

        adminMdl.update(
          { token: req.body.token },
          { $set: { password: newpassword } },
          function(err, updated) {
            if (err) {
              throw err;
            }
            return res.success({ updated }, "Password updated successfully");
          }
        );
      } else {
        return res.status(400).failure("Invalid Token");
      }
    });
  } else {
    return res.status(400).failure("Token is required");
  }
};

var forgetPasswordAction = function(req, res) {
  if (req.body.email && req.body.email != "") {
    var email = req.body.email
      .toString()
      .toLowerCase()
      .trim();
  } else {
    return res.status(400).failure("please Enter email");
  }

  adminMdl.findOne({ email: email }).exec(function(err, data) {
    if (err) throw err;
    if (!data) {
      return res.status(400).failure("No Such Email Id Exists");
    } else {
      var emailtransport = nodemailer.createTransport({
        service: "Gmail",
        auth: {
          user: "piyushkapoor786@gmail.com",
          pass: "P!yush@1994"
        }
      });

      var mailOptions = {
        from: "piyushkapoor786@gmail.com",
        to: email,
        subject: "Forget Password | Wedkart Admin",
        text: `<p>Someone Requested a password from your motai admin account<br>Your Password is <h2>${
          data.password
        }</h2> <br> If you have not make this request than you can successfully ignore this.<br> Thank You. <br></p>`
      };

      emailtransport.sendMail(mailOptions, function(error, info) {
        if (error) throw err;
        else console.log("Email Sent Successfuully");
      });
      return res.success(
        { email: email },
        "Your password is sent to your mail"
      );
    }
  });
};

var addCategory = function(req, res) {
  var token = req.body.token;
  adminMdl.find({ token: token }).exec(function(err, adminData) {
    if (adminData && adminData.length > 0) {
      var cat = {};
      // var sub_cat = {}
      if (req.body.name && req.body.name != "") {
        cat.name = req.body.name.toString().trim();
      } else {
        return res.status(400).failure("please Enter category");
      }
      if (req.body.sub_cat && req.body.sub_cat != "") {
        cat.subcategories = req.body.sub_cat.toString().trim();
      }

      categoryMdl
        .findOne({ name: cat.name, is_deleted: false })
        .exec(function(err, catData) {
          if (err) throw err;
          if (catData) {
            return res.status(400).failure("category already Exist");
          }

          category = new categoryMdl(cat);

          category.save();
          console.log(catData);
          console.log(category);

          return res
            .status(200)
            .success(category, "category successfully Added");
        });
    } else {
      return res.status(400).failure("Invalid Token");
    }
  });
};

function getCategories(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }
  var token = req.headers["x-auth-token"].trim();

  adminMdl.findOne({ token: token }).exec(function(err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No User found.");
    if (data) {
      categoryMdl.find().exec((err, category) => {
        if (err) throw err;
        else if (!category.length)
          return res.status(400).failure("No category found.");
        else {
          console.log("cat", category);
          return res.status(200).success(category);
        }
      });
    }
  });
}

function getCategory(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  var catId = req.params.cat_id.trim();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }
  var token = req.headers["x-auth-token"].trim();

  adminMdl.findOne({ token: token }).exec(function(err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No User found.");
    if (data) {
      categoryMdl
        .find({ _id: catId, is_deleted: false })
        .exec((err, category) => {
          if (err) throw err;
          else if (!category.length)
            return res.status(400).failure("No category found.");
          else {
            console.log("cat", category);
            return res
              .status(200)
              .success(category, "category get successfully");
          }
        });
    }
  });
}

function deleteCategory(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }
  var token = req.headers["x-auth-token"].trim();
  var catId = req.body.cat_id.trim();
  adminMdl.findOne({ token: token }).exec(function(err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No User found.");
    if (data) {
      categoryMdl
        .update({ _id: catId }, { $set: { is_deleted: true } })
        .exec((err, category) => {
          if (err) throw err;
          else if (!Object.keys(category).length)
            return res.status(400).failure("No category found.");
          else {
            console.log("cat", category);
            return res
              .status(200)
              .success(category, "category delete successfully");
          }
        });
    }
  });
}

function updateCategory(req, res) {
  req.checkHeaders("x-auth-token", "Invalid Token").exists();
  var cat = {};
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }
  if (req.body.name && req.body.name != "") {
    cat.name = req.body.name.toString().trim();
  } else {
    return res.status(400).failure("please Enter category");
  }
  if (req.body.sub_cat && req.body.sub_cat != "") {
    cat.subcategories = req.body.sub_cat.toString().trim();
  }
  var token = req.headers["x-auth-token"].trim();
  var catId = req.body.cat_id.trim();
  adminMdl.findOne({ token: token }).exec(function(err, data) {
    if (err) throw err;
    if (!data) return res.status(400).failure("No User found.");
    if (data) {
      console.log(catId);
      console.log(cat);
      categoryMdl
        .update({ _id: catId }, { $set: cat })
        .exec((err, category) => {
          if (err) throw err;
          else if (!Object.keys(category).length)
            return res.status(400).failure("No category found.");
          else {
            console.log("cat", category);
            return res
              .status(200)
              .success(category, "category delete successfully");
          }
        });
    }
  });
}

var admin = {
  login: loginAction,
  logout: logoutAction,
  info: infoAction,
  auth: authAction,
  forgetPassword: forgetPasswordAction,
  changePassword: changePasswordAction,
  getCategory: getCategory,
  getCategories: getCategories,
  deleteCategory: deleteCategory,
  addCategory: addCategory,
  updateCategory: updateCategory
};

module.exports = admin;
