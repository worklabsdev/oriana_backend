var adminMdl = require('../../models/admin.js')
var userMdl = require('../../models/buyer.js')
var jsend = require('../../plugins/jsend.js')
var nodemailer = require("nodemailer");

//var emailtransport = require('../../config/email.js');


var addAction = function (req, res) {
	var token = req.body.token
	adminMdl.find({ 'token': token }).exec(function (err, adminData) {
		if (adminData && adminData.length > 0) {
			var user = {}
			if (req.body.name && req.body.name != "") {
				user.name = req.body.name.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter name')
			}

			if (req.body.email && req.body.email != "") {
				user.email = req.body.email.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter Email')
			}

			if (req.body.phone && req.body.phone != "") {
				user.phone = req.body.phone.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter Phone Number')

			}

			var password = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for (var i = 0; i < 8; i++) {
				password += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			user.password = password
			

			userMdl.findOne({ 'email': user.email, is_deleted: false }).exec(function (err, userData) {
				if (err) throw err;
				if (userData) {
					return res.status(400).failure('Email already Exist')

				}

				user = new userMdl(user);
				user.save()

				var emailtransport = nodemailer.createTransport({
					service: "Gmail",
					auth: {
					  user: "piyushkapoor786@gmail.com",
					  pass: "P!yush@1994"
					}
				  });

				var mailOptions = {
					from: 'piyushkapoor786@gmail.com',
					to: user.email,
					subject: 'User Registered | Wedkart',
					text: `<p>Your motai account has succesfully added by admin. You can now enjoy our services. <br>Your Password is <h2>${password}</h2> <br></p>`
				  };
				  
				  emailtransport.sendMail(mailOptions, function(error, info){
					if (error) throw err;
					else console.log('Email Sent Successfuully')
				  });

				  return res.success(userData,'User successfully Added')

			})
		}
		else {
			return res.status(400).failure('Invalid Token')
		}
	})
}

var deleteAction = function (req, res) {
	var token = req.body.token
	adminMdl.find({ 'token': token }).exec(function (err, adminData) {
		if (adminData && adminData.length > 0) {
			if (req.body._id && req.body._id != "") {
				var userId = req.body._id.toString().trim()
			}
			else {
				return res.status(400).failure('InvalidTokens')
			}

			userMdl.update({ '_id': userId }, { $set: { 'isDeleted': true } }).exec(function (err, data) {
				if (err) throw err;
				if(data){
					return res.success(data,'User successfully Deleted')
				 }
			})
		}
		else {
			return res.status(400).failure('InvalidTokens')
		}
	})
}

var updateAction = function (req, res) {
	var token = req.body.token
	adminMdl.find({ 'token': token }).exec(function (err, adminData) {
		if (adminData && adminData.length > 0) {
			var user = {}
			if (req.body.name && req.body.name != "") {
				user.name = req.body.name.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter name')
			}

			if (req.body.email && req.body.email != "") {
				user.email = req.body.email.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter Email')
			}

			if (req.body.phone && req.body.phone != "") {
				user.phone = req.body.phone.toString().trim()
			}
			else {
				return res.status(400).failure('please Enter Phone Number')
			}
			if (req.body._id && req.body._id != "") {
				var userId = req.body._id.toString().trim()
			}
			else {
				return res.status(400).failure('User Id Not Found')
			}

			userMdl.findOne({ 'email': user.email, is_deleted: false, '_id': { $ne: userId } }).exec(function (err, data) {
				if(err) throw err
				if (data) {
					return res.status(400).failure('Email already Exist')
				}

				userMdl.update({ '_id': userId }, { $set: user }).exec(function (err, data) {
					if (err) throw err;
					if(data){
						return res.success(data,'Motoboy successfully Updated')
					 }
				})
			})
		}
		else {
			return res.status(400).failure('Invalid Token')
		}
	})
}

var motoboysActivate = function (req, res) {
	var token = req.body.token
	adminMdl.find({ 'token': token }).exec(function (err, adminData) {
		if (adminData && adminData.length > 0) {
			if (req.body.user_id && req.body.user_id != "") {
				var userId = req.body.user_id.toString().trim()
			}
			else {
				return res.status(400).failure('Motoboy ID Not Found', {
					'errors': err
				})
			}

			userMdl.update({ '_id': userId }, { $set: { 'isBlocked': false } }).exec(function (err, data) {
				if (err) throw err;
				if(data){
					return res.success(data,'User successfully Activated')
				 }
			})
		}
		else {
			return res.status(400).failure('InvalidTokens', {
				'errors': err
			})
		}
	})
}

var motoboysInactivate = function (req, res) {
	var token = req.body.token
	adminMdl.find({ 'token': token }).exec(function (err, adminData) {
		if (adminData && adminData.length > 0) {
			if (req.body.user_id && req.body.user_id != "") {
				var userId = req.body.user_id.toString().trim()
			}
			else {
				return res.status(400).failure('Motoboy ID Not Found', {
					'errors': err
				})
			}

			userMdl.update({ '_id': userId }, { $set: { 'isBlocked': true } }).exec(function (err, data) {
				if (err) throw err;
				if(data){
					return res.success(data,'User successfully In-Activated')
				 }
			})
		}
		else {
			return res.status(400).failure('InvalidTokens', {
				'errors': err
			})
		}
	})
}


var listAction = function (req, res) {
	var token = req.body.token
	adminMdl.findOne({ 'token': token }).exec(function (err, adminData) {
		if (adminData) {
			let currPage = req.body.page ? req.body.page-1 : 0
			let filters = req.body.filters ? req.body.filters : ''


			userMdl.find({ isDeleted: false, name : new RegExp(filters,'i') }).skip(currPage*10).limit(10).sort({ 'create_time': -1 }).exec(function (err, usersData) {
				if (err) throw err
				if (usersData.length < 1) {
					return res.status(400).failure('No User Found', {
						'errors': err
					})
				}			
				userMdl.find({ isDeleted: false, name : new RegExp(filters,'i') }).count().exec(function (err, userCount) {
					if (err) throw err	
					return res.success({ 'list': usersData, 'totalPage': Math.ceil(userCount/10) , 'currPage' : currPage+1})

				})
			})
		}
		else {
			return res.status(400).failure('Invalid Token', {
				'errors': err
			})
			
		}
	})
}

var getAction = function (req, res) {
	var token = req.body.token
	adminMdl.findOne({ 'token': token }).exec(function (err, adminData) {
		if (adminData) {

			if (req.body.user_id && req.body.user_id != "") {
				var userId = req.body.user_id.toString().trim()
			}
			else {
				return res.status(400).failure('Motoboy ID Not Found', {
					'errors': err
				})
			}
console.log(userId)
			userMdl.findOne({ _id: userId, isDeleted: false }).exec(function (err, userData) {
				if (err) throw err
				if (!userData) {
					return res.status(400).failure('No User Found', {
						'errors': err
					})
				}
				if(userData){
					return res.success(userData,'User successfully get')
				 }
			})
		}
		else {
			return res.status(400).failure('Invalid Token', {
				'errors': err
			})
		}
	})
}

var motoboyObj = {
	'add': addAction,
	'delete': deleteAction,
	'update': updateAction,
	'activate': motoboysActivate,
	'inactivate': motoboysInactivate,
	'list': listAction,
	'get': getAction,
}

module.exports = motoboyObj
