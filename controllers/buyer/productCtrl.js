var async = require("async");
var productMdl = require("../../models/product");
var buyerMdl = require("../../models/buyer");
var categoryMdl = require("../../models/categories");
var subcategoryMdl = require("../../models/subcategories");

function getCategory(req, res) {
  categoryMdl.find().exec((err, category) => {
    if (err) throw err;
    else if (!category.length)
      return res.status(400).failure("No category found.");
    else {
      res.success(category);
    }
  });
}

function getsubcategory(req, res) {
  req.checkParams("catid", "No id was found").exists();

  var error = req.validationErrors();
  if (error) {
    return res.status(400).failure("Error", { errors: errors });
  }
  subcategoryMdl.find({ cat_id: req.params.catid }).exec((err, catData) => {
    if (err) throw err;
    else if (!catData.length) return res.status(400).failure("No User found.");
    else {
      res.success(catData);
    }
  });
}

function allProductAction(req, res) {
  productMdl.find().exec((err, allProducts) => {
    if (err) throw err;
    return res.status(200).success(allProducts);
  });
}

function getCategoryProd(req, res) {
  req.checkBody("category", "Invalid Category Id").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      errors: errors
    });
  }
  productMdl.find({ category: req.body.category }).exec((err, allProducts) => {
    if (err) throw err;
    return res.status(200).success(allProducts);
  });
}

function getsubcategoryProd(req, res) {
  req.checkBody("subcategory", "Invalid Sub Category Id").exists();

  productMdl
    .find({ subcategory: req.body.subcategory })
    .exec((err, allProducts) => {
      if (err) throw err;
      return res.status(200).success(allProducts);
    });
}

function getProductAction(req, res) {
  req.checkParams("id", "Invalid Product Id").exists();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", {
      errors: errors
    });
  }
  productMdl
    .findOne({
      _id: req.params.id
    })
    .populate("seller.id")
    .exec(
      function(err, data) {
        if (err) throw err;
        if (!data) return res.status(400).failure("No Such Product Found");
        return res.status(200).success(data);
      },
      err => {
        if (err) {
          console.log(err);
        }
      }
    );
}

function dashboard(req, res) {
  categoryMdl.find().exec((err, category) => {
    if (err) throw err;
    else if (!category.length)
      return res.status(400).failure("No category found.");
    else {
      res.success(category);
    }
  });

  productMdl.find().exec((err, allProducts) => {
    if (err) throw err;
    return res.status(200).success(allProducts);
  });
}

function buyersearch(req, res) {
  console.log(req.body);
  req.checkBody("search", "No Product was found").exists();
  var regex = new RegExp(escapeRegex(req.body.search), "gi");
  productMdl
    .find({ name: { $regex: req.body.search, $options: "i" } })
    .exec((err, searchdata) => {
      console.log("getting results", searchdata);
      if (err) res.failure(err);
      else if (!searchdata) return res.status(400).failure("No products found");
      else
        res.json({
          status: 200,
          data: searchdata,
          message: "Products Listed Here"
        });
    });
}

function escapeRegex(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

module.exports = {
  allProduct: allProductAction,
  getCategory: getCategory,
  getsubcategory: getsubcategory,
  getCategoryProd: getCategoryProd,
  getsubcategoryProd: getsubcategoryProd,
  getProduct: getProductAction,
  dashboard: dashboard,
  buyersearch: buyersearch
};
